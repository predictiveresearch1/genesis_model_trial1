# balanced train data and test data used
# all the 32 features used
# checking logistic regression, xgbooster,randomforest classifier, svm classifier models

import pandas as pd
from sklearn.metrics import confusion_matrix

#reading the balanced preprocessed data
data = pd.read_csv('genesis_processed_bal.csv')
# filling empty cells with 0
data= data.fillna(0)

print('Actual data',len(data))
print(data.columns)

converted_data = data.loc[data['LeadConverted'] == 1]
not_converted_data = data.loc[data['LeadConverted'] == 0]

# Balancing the train data
train_convert = converted_data.head(2048)
train_not_convert = not_converted_data.head(2048)

test_convert = converted_data[~converted_data.UID.isin(train_convert.UID)]
test_not_convert = not_converted_data[~not_converted_data.UID.isin(train_not_convert.UID)]


train_data = pd.concat([train_convert,train_not_convert],axis=0)
test_data = pd.concat([test_convert,test_not_convert],axis=0)
# print(type(test_data))
# test_data.to_csv('test_data.csv',index=False)


# separating the features and targets
train_target = data['LeadConverted']
train_features = data.drop(['UID','LeadConverted','GroupID', 'HouseholdID'],axis = 1)

test_target = data['LeadConverted']
test_features = data.drop(['UID','LeadConverted','GroupID', 'HouseholdID'],axis = 1)
print('\n*Number of Features*',len(test_features.columns))
y_train = train_target
y_test = test_target


# converting categorical values into numeric values
for value in train_features.columns:
       train_features[value] = train_features[value].astype('category').cat.codes

for value in test_features.columns:
       test_features[value] = test_features[value].astype('category').cat.codes


# scaling the features
from sklearn.preprocessing import MinMaxScaler
scaler = MinMaxScaler()
scaler.fit(train_features)
x_train = scaler.transform(train_features)

scaler1 = MinMaxScaler()
scaler1.fit(test_features)
x_test = scaler.transform(test_features)

# from sklearn.model_selection import train_test_split
# # split the data for testing & training
# x_train, x_test, y_train, y_test = train_test_split(features, target, test_size=0.3, random_state=1)


#LogisticRegression model
from sklearn.linear_model import LogisticRegression
lrc = LogisticRegression()
# training the model
lrc.fit(x_train, train_target)
#predicting the results
lrc_pred = lrc.predict(x_test)
print('\nconfusion_matrix...',confusion_matrix(y_test, lrc.predict(x_test)))
# pd.DataFrame(lrc_pred).to_csv('logistic_prediction.csv',index=False)
#calculating accuracy
from sklearn.metrics import accuracy_score
lrc_accuracy = accuracy_score(y_test, lrc_pred)
print("\n \n Logistic Regression Accuracy: {:.2f}%".format(lrc_accuracy * 100))



#XGBClassifier model
print('\n\n*****XGBClassifier*****')
from xgboost import XGBClassifier
xgbc = XGBClassifier(silent=False, scale_pos_weight=1, learning_rate=0.01, colsample_bytree=0.4, subsample=0.8,
                     objective='binary:logistic', n_estimators=1000, reg_alpha=0.3, max_depth=4, gamma=4)
# training the model
xgbc.fit(x_train, y_train)
print('\nconfusion_matrix...',confusion_matrix(y_test, xgbc.predict(x_test)))
xgbc_pred = lrc.predict(x_test)
# pd.DataFrame(xgbc_pred).to_csv('xgb_prediction.csv',index=False)
#calculating accuracy of the model
print('\n XGBClassifier accuracy_score...',accuracy_score(y_test, xgbc_pred))
# print('\nclassification_report...',classification_report(y_test, clf.predict(x_test)))



# Random Forest Model
print('\n\n*****RFClassifier*****')
from sklearn.ensemble import RandomForestClassifier
#Create a Gaussian Classifier
rfc=RandomForestClassifier(n_estimators=100)
#Training the model
rfc.fit(x_train, y_train)
# predicting the results
rfc_pred=rfc.predict(x_test)
print('\nconfusion_matrix...',confusion_matrix(y_test, rfc_pred))

from sklearn import metrics
# Model Accuracy
print("Random Forest Accuracy:",metrics.accuracy_score(y_test, rfc_pred))
# pd.DataFrame(xgbc_pred).to_csv('rfc_prediction.csv',index=False)



# svm model
print('\n\n*****SVMClassifier*****')
from sklearn import svm
#Create an svm Classifier
svmc = svm.SVC(kernel='linear',gamma=0.2) # Linear Kernel
#training the model
svmc.fit(x_train, y_train)
#Predicting the results
svm_pred = svmc.predict(x_test)
print('\nconfusion_matrix...',confusion_matrix(y_test, svm_pred))
#Import scikit-learn metrics module for accuracy calculation
from sklearn import metrics
# Model Accuracy: how often is the classifier correct?
print("SVM Accuracy:",metrics.accuracy_score(y_test, svm_pred))
# pd.DataFrame(xgbc_pred).to_csv('svm_prediction.csv',index=False)


test_data ['logistic_prediction'] = pd.DataFrame(lrc_pred)
test_data ['xgb_prediction'] = pd.DataFrame(xgbc_pred)
test_data ['rfc_prediction'] = pd.DataFrame(rfc_pred)
test_data ['svm_prediction'] = pd.DataFrame(svm_pred)

test_data.to_csv('case1_test_data.csv',index=False)
