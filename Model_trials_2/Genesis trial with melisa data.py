# balanced train data and test data used
# all the 32 features used
# checking logistic regression, xgbooster,randomforest classifier, svm classifier models

import pandas as pd
from sklearn.metrics import confusion_matrix

raw_data = pd.read_csv('E:/Python_Project_Directory/Genesis_Pipeline_2/code/genesis_preprocessed_data.csv')
print('Actual Data',len(raw_data))
print(raw_data.columns)
train_date = ['2019-01', '2019-02', '2019-03', '2019-04', '2019-05', '2019-06', '2019-07', '2019-08', '2019-09', '2019-10', '2019-11', '2019-12', '2020-01', '2020-02', '2020-03', '2020-04', '2020-05', '2020-06', '2020-07', '2020-08', '2020-09', '2020-10', '2020-11', '2020-12','2021-01','2021-02','2021-03']
test_date = ['2021-04','2021-05','2021-06']


train_data = raw_data["Period"].isin(train_date)
train_data = raw_data[train_data]
# print(len(train_data))
test_data = raw_data["Period"].isin(test_date)
test_data = raw_data[test_data]
# print(len(test_data))

# print('train_data',train_data['Period'].unique())
# print('test_data',test_data['Period'].unique())

converted_data = train_data.loc[train_data['LeadConverted'] == 1]
not_converted_data = train_data.loc[train_data['LeadConverted'] == 0]
print('converted_data',len(converted_data))
print('not converted_data',len(not_converted_data))

# undersampling
data_len = 0
if len(converted_data) > len(not_converted_data):
    data_len = len(not_converted_data)
elif len(converted_data) < len(not_converted_data):
    data_len = len(converted_data)

import random
# not_converted_data = random.sample(not_converted_data, data_len)
converted_data = converted_data.sample(data_len)
not_converted_data = not_converted_data.sample(data_len)

# print(len(converted_data))
# print(len(not_converted_data))
# input()
train_data = pd.concat([converted_data,not_converted_data],axis=0)
# print(train_data.columns)
# input()

train_data.to_csv('balanced_data.csv',index=False)
# test_data.to_csv('case1_test_data.csv',index=False)
# train_data =pd.read_csv('case1_train_data.csv')
# test_data =pd.read_csv('case1_test_data.csv')


# separating the features and targets
train_target = train_data['LeadConverted']
train_features = train_data.drop(['UID','LeadConverted','GroupID', 'HouseholdID', 'TestDriveAddress','Period'],axis = 1)

###############################

converted_data = test_data.loc[test_data['LeadConverted'] == 1]
not_converted_data = test_data.loc[test_data['LeadConverted'] == 0]
print('converted_data',len(converted_data))
print('not converted_data',len(not_converted_data))

# undersampling
data_len = 0
if len(converted_data) > len(not_converted_data):
    data_len = len(not_converted_data)
elif len(converted_data) < len(not_converted_data):
    data_len = len(converted_data)

import random
# not_converted_data = random.sample(not_converted_data, data_len)
converted_data = converted_data.sample(data_len)
not_converted_data = not_converted_data.sample(data_len)

test_data = pd.concat([converted_data,not_converted_data],axis=0)
# test_data.to_csv('test_data.csv',index=False)
###############################

test_target = test_data['LeadConverted']
test_features = test_data.drop(['UID','LeadConverted','GroupID', 'HouseholdID', 'TestDriveAddress','Period'],axis = 1)
print('\n*Number of Features*',len(test_features.columns))
y_train = train_target
y_test = test_target
x_train = train_features
x_test = test_features


# from sklearn.model_selection import train_test_split
# # split the data for testing & training
# x_train, x_test, y_train, y_test = train_test_split(features, target, test_size=0.3, random_state=1)


#LogisticRegression model
from sklearn.linear_model import LogisticRegression
lrc = LogisticRegression()
# training the model
lrc.fit(x_train, y_train)
#predicting the results
lrc_pred = lrc.predict(x_test)
print('\nconfusion_matrix...',confusion_matrix(y_test, lrc.predict(x_test)))
# pd.DataFrame(lrc_pred).to_csv('logistic_prediction.csv',index=False)
#calculating accuracy
from sklearn.metrics import accuracy_score
lrc_accuracy = accuracy_score(y_test, lrc_pred)
print("\n \n Logistic Regression Accuracy: {:.2f}%".format(lrc_accuracy * 100))



#XGBClassifier model
print('\n\n*****XGBClassifier*****')
from xgboost import XGBClassifier
xgbc = XGBClassifier(silent=False, scale_pos_weight=1, learning_rate=0.01, colsample_bytree=0.4, subsample=0.8,
                     objective='binary:logistic', n_estimators=1000, reg_alpha=0.3, max_depth=4, gamma=4)
# training the model
xgbc.fit(x_train, y_train)
print('\nconfusion_matrix...',confusion_matrix(y_test, xgbc.predict(x_test)))
xgbc_pred = lrc.predict(x_test)
# pd.DataFrame(xgbc_pred).to_csv('xgb_prediction.csv',index=False)
#calculating accuracy of the model
print('\n XGBClassifier accuracy_score...',accuracy_score(y_test, xgbc_pred))
# print('\nclassification_report...',classification_report(y_test, clf.predict(x_test)))



# Random Forest Model
print('\n\n*****RFClassifier*****')
from sklearn.ensemble import RandomForestClassifier
#Create a Gaussian Classifier
rfc=RandomForestClassifier(n_estimators=100)
#Training the model
rfc.fit(x_train, y_train)
# predicting the results
rfc_pred=rfc.predict(x_test)
print('\nconfusion_matrix...',confusion_matrix(y_test, rfc_pred))

from sklearn import metrics
# Model Accuracy
print("Random Forest Accuracy:",metrics.accuracy_score(y_test, rfc_pred))
# pd.DataFrame(xgbc_pred).to_csv('rfc_prediction.csv',index=False)



# svm model
print('\n\n*****SVMClassifier*****')
from sklearn import svm
#Create an svm Classifier
svmc = svm.SVC(kernel='linear',gamma=0.2,C=1) #training the model ) # Linear Kernel
#training the model
svmc.fit(x_train, y_train)
#Predicting the results
svm_pred = svmc.predict(x_test)
print('\nconfusion_matrix...',confusion_matrix(y_test, svm_pred))
#Import scikit-learn metrics module for accuracy calculation
from sklearn import metrics
# Model Accuracy: how often is the classifier correct?
print("SVM Accuracy:",metrics.accuracy_score(y_test, svm_pred))
# pd.DataFrame(xgbc_pred).to_csv('svm_prediction.csv',index=False)


test_data ['logistic_prediction'] = pd.DataFrame(lrc_pred)
test_data ['xgb_prediction'] = pd.DataFrame(xgbc_pred)
test_data ['rfc_prediction'] = pd.DataFrame(rfc_pred)
test_data ['svm_prediction'] = pd.DataFrame(svm_pred)

# test_data.to_csv('case1_trial1_test_data.csv',index=False)
print('Without Melisa variable zip_code_wt, MedianHomeValue')
print("\nLogistic Regression Accuracy: {:.2f}%".format(lrc_accuracy * 100))
print('XGBClassifier accuracy_score ',accuracy_score(y_test, xgbc_pred) * 100)
print("Random Forest Accuracy:      ",metrics.accuracy_score(y_test, rfc_pred) * 100)
print("SVM Accuracy:                ",metrics.accuracy_score(y_test, svm_pred) * 100)